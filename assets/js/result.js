$(function(){
  var animation_class = [
        'vanishIn',
        'swap',
        'swashIn',
        'foolishIn',
        'twisterInUp',
        'twisterInDown',
        'puffIn',
        'spaceInDown',
        'tinUpIn'
      ];
      
  var rand_class = animation_class[Math.floor(Math.random() * animation_class.length)];
  var dram = $("#dram").get(0);
  dram.volume = 1;
  
  setTimeout(function(){
    dram.pause();
    $('.present-box .close-box').animate({"opacity": 0}, 900, 'linear');
    $('.present-box .close-box').removeClass('shake-slow');
  },3000);
  
  var sound = $("#sound").get(0);
  
  setTimeout(function(){
    sound.play();
    //$('.result-item img').animate({"opacity": 1}, 900, 'linear');
    $('.result-item').addClass('time ' + rand_class);
  },3500);
  
  setTimeout(function(){
    window.location.href = '/';
  },18000);
});
