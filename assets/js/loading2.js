$(function(){
  // プレゼントBOXの座標を取得
  var house_top = $('.house-box').offset().top; 
  var house_left = $('.house-box').offset().left;
  console.log('left:' + house_left);
  
  var music = $('#music').get(0);
  music.volume = 0.4;
  
  $('.santa-box').css({right: (house_left + 52) + 'px'});
  
  setTimeout(function(){
      $('#bell').get(0).pause()
  }, 1000);
    
    // サンタクロースが到着後、プレゼントBOXを落とす
    setTimeout(function(){
        $('.present-box').addClass('rotation')
    }, 1500);
    
    // サンタクロースがプレゼントBOXに到着したら、プレゼントBOXを落とす処理
    setTimeout(function(){
        $('.santa-box').animate({"right": 5000 + 'px'}, 1000, 'linear');
    }, 5000);
    
    var audio = $('#don').get(0);
    
    setTimeout(function(){
        
        audio.volume = 1,
        audio.play()
    }, 4500);
    
    // ページ移動
    setTimeout(function(){
        window.location.href = 'result2.php';
    }, 7500);
});
