$(function(){
  // プレゼントBOXの座標を取得
  var present_top = $('.present-box').offset().top; 
  var present_left = $('.present-box').offset().left;
  
  var baseVol = 0.5; // ベース音量
  var fadeSpeed = 1500; // スピード
  var audio = new Audio();
  audio.src = 'assets/music/christmas.MP3';
  
  var media = document.getElementById("bell");
  
  // 音
/*   $('#bell').get(0).play(); */
  
  $('.btn-present').click(function() { //プレゼントBOXクリックしたらイベント発火
      console.log('クリックしました。');
      $('.arrow-image-box img').removeClass('infinite'); // 矢印のアニメーション停止
      $('.text-image').addClass('animated fadeOut'); // 「押してね」のテキストをフェードアウト
      $('.arrow-image-box img').addClass('fadeOut'); // 矢印をフェードアウト
      
      setTimeout(function(){
          $('#bell').get(0).play()
      }, 900);
      
      $('.santa-box').animate({"left": (present_left - 552) + 'px'}, 2500, 'linear'); // サンタクロースをツリーまで移動
      
      setTimeout(function(){
          $('#bell').get(0).pause();
      }, 2400);
      
      // サンタクロースがプレゼントBOXに到着したら、プレゼントBOXを落とす処理
      setTimeout(function(){
          $('.present-box').addClass('rotation');
      }, 3000);
      
      // ボタン押下して5秒後にサンタクロースを左に移動
      setTimeout(function(){
          $('.santa-box').stop().addClass('time tinLeftOut')
      }, 5000);
      
      setTimeout(function(){
          $('#bell').get(0).play()
      }, 6000);
      
      // ページ移動
      setTimeout(function(){
          window.location.href = './loading.php';
      }, 7500);
  })
});
