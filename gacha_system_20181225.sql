-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2018 年 12 月 25 日 01:43
-- サーバのバージョン： 5.6.42
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gacha_system`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `count_data`
--

CREATE TABLE `count_data` (
  `id` int(11) NOT NULL,
  `count` int(11) NOT NULL COMMENT 'くじ引き回数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='くじの回転数テーブル';

--
-- テーブルのデータのダンプ `count_data`
--

INSERT INTO `count_data` (`id`, `count`) VALUES
(1, 16);

-- --------------------------------------------------------

--
-- テーブルの構造 `shohin_data`
--

CREATE TABLE `shohin_data` (
  `id` int(11) NOT NULL COMMENT '商品id',
  `name` varchar(255) NOT NULL COMMENT '商品名',
  `item_image` varchar(255) NOT NULL COMMENT '商品画像',
  `reality` double(5,2) NOT NULL COMMENT 'レア率（重み）',
  `flag` int(1) NOT NULL COMMENT '在庫フラグ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品データが格納されているテーブル';

--
-- テーブルのデータのダンプ `shohin_data`
--

INSERT INTO `shohin_data` (`id`, `name`, `item_image`, `reality`, `flag`) VALUES
(1, '東京ディズニーペアチケットorUSJペアチケット', '/assets/images/item_1.jpg', 4.23, 1),
(2, '松阪牛すき焼き 約300g', '/assets/images/item_2.jpg', 4.23, 1),
(3, 'ハーゲンダッツ&フルーツティアラセット', '/assets/images/item_3.jpg', 4.23, 1),
(4, '今治謹製 極上タオル フェイスタオル', '/assets/images/item_4.jpg', 4.23, 1),
(5, 'AGF&ドトール バラエティギフト', '/assets/images/item_5.jpg', 4.23, 1),
(6, 'ボディソープ&薬用入浴剤セット', '/assets/images/item_6.jpg', 4.23, 1),
(7, 'フロッシュ キッチン洗剤ギフト', '/assets/images/item_7.jpg', 4.23, 1),
(8, 'ユーカリーハーモニー タオルセット', '/assets/images/item_8.jpg', 4.23, 1),
(9, 'アンナ・マリオネット コーヒーヒメ10P', '/assets/images/item_9.jpg', 4.23, 1),
(10, 'カゴメ野菜生活ギフト', '/assets/images/item_10.jpg', 4.23, 1),
(11, '神戸トラッドクッキー', '/assets/images/item_11.jpg', 4.23, 1),
(12, '手延三輪素麺', '/assets/images/item_12.jpg', 4.23, 0),
(13, '牛乳石鹸 カウブランド青箱2個入り', '/assets/images/item_13.jpg', 4.23, 1),
(14, 'ハズレ', '/assets/images/item_14.png', 45.00, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `stock_data`
--

CREATE TABLE `stock_data` (
  `id` int(11) NOT NULL COMMENT '商品id',
  `stock` int(11) NOT NULL COMMENT '商品の在庫数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品在庫情報テーブル';

--
-- テーブルのデータのダンプ `stock_data`
--

INSERT INTO `stock_data` (`id`, `stock`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 2),
(13, 0),
(14, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `count_data`
--
ALTER TABLE `count_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shohin_data`
--
ALTER TABLE `shohin_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_data`
--
ALTER TABLE `stock_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `count_data`
--
ALTER TABLE `count_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shohin_data`
--
ALTER TABLE `shohin_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id', AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `stock_data`
--
ALTER TABLE `stock_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id', AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
