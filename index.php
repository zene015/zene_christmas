<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8" />
    <title>クリスマス用ガチャ</title>
    <link rel="stylesheet" href="assets/css/animate.min.css" />
    <link rel="stylesheet" href="assets/css/magic.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="assets/js/snowfall.jquery.min.js"></script>
</head>
<body>
    <div class="loading">
        
    </div>
    <div class="top">
        <div class="tree-box">
            <div class="induction-box">
                <img src="assets/images/text-image.png" alt="押してね♪" class="text-image"/>
                <div class="arrow-image-box"><img src="assets/images/right-arrow.png" alt="右矢印" class="arrow-image animated rubberBand infinite"/></div>
            </div>
            <img src="assets/images/tree_image.png" alt="クリスマスツリー" class="tree-position"/>
            <img src="assets/images/present_image.png" alt="プレゼントBOX" class="present-box btn-present"/>
        </div>
        <div class="santa-box">
            <img src="assets/images/santa_image.png" alt="サンタクロース" class="santa"/>
        </div>
    </div>
    <audio id="music" preload="auto" loop autoplay>
        <source src="assets/music/christmas.MP3" type="audio/mp3">
    </audio>
    <audio id="bell" preload="auto">
        <source src="assets/music/christmasbell.mp3" type="audio/mp3">
    </audio>
</body>
<script>
$(function() {
    $('html').snowfall({
        flakeCount: 25,
        minSize: 5,
        maxSize: 30,
        minSpeed: 2,
        maxSpeed: 6,
        image: "assets/images/snow.png",
    });
});
</script>
<script src="assets/js/top.js"></script>
</html>
