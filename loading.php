<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8" />
    <title>プレゼント アニメーション</title>
    <link rel="stylesheet" href="assets/css/animate.min.css" />
    <link rel="stylesheet" href="assets/css/magic.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="assets/js/snowfall.jquery.min.js"></script>
</head>
<body>
    <div class="back">
        <div class="house-box">
            <img src="assets/images/house_image.png" alt="家" class="house-position"/>
        </div>
        <div class="santa-box time tinRightIn">
            <img src="assets/images/santa_image.png" alt="サンタクロース" class="santa"/>
            <img src="assets/images/present_image.png" alt="プレゼントBOX" class="present-box"/>
        </div>
    </div>
    <audio id="music" preload="auto" loop autoplay>
        <source src="assets/music/christmas.MP3" type="audio/mp3">
    </audio>
    <audio id="bell" preload="auto" loop autoplay>
        <source src="assets/music/christmasbell.mp3" type="audio/mp3">
    </audio>
    <audio id="don" preload="auto">
        <source src="assets/music/don-1.mp3" type="audio/mp3">
    </audio>
</body>
<script>
$(function() {
    $('html').snowfall({
        flakeCount: 25,
        minSize: 5,
        maxSize: 30,
        minSpeed: 2,
        maxSpeed: 6,
        image: "assets/images/snow.png",
    });
});
</script>
<script src="assets/js/loading.js"></script>
</html>
