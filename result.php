<?php require_once('./item_output.php'); ?>
<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8" />
    <title>プレゼント結果ページ</title>
    <link rel="stylesheet" href="assets/css/animate.min.css" />
    <link rel="stylesheet" href="assets/css/csshake.min.css" />
    <link rel="stylesheet" href="assets/css/magic.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="assets/js/snowfall.jquery.min.js"></script>
</head>
<body>
    <div class="animation"> 
        <div class="present-box">
            <img src="assets/images/present_image.png" alt="クローズプレゼントBOX" class="close-box shake-constant shake-slow"/>
        </div>
        <div class="result-box">
            <div class="result-item">
                <?php if($item_name != 'ハズレ'): ?>
                    <?php if($id == 1): ?>
                    <audio id="sound" preload="auto">
                        <source src="assets/music/shine1.mp3" type="audio/mp3">
                    </audio>
                    <?php elseif($id == 2): ?>
                    <audio id="sound" preload="auto">
                        <source src="assets/music/shine3.mp3" type="audio/mp3">
                    </audio>
                    <?php else: ?>
                    <audio id="sound" preload="auto">
                        <source src="assets/music/trumpet1.mp3" type="audio/mp3">
                    </audio>
                    <?php endif; ?>
                <img src="<?php echo $item_image; ?>" alt="<?php echo $item_name; ?>" />
                <?php else: ?>
                <audio id="sound" preload="auto">
                    <source src="assets/music/tin2.mp3" type="audio/mp3">
                </audio>
                <img src="<?php echo $item_image; ?>" alt="<?php echo $item_name; ?>" class~="missing-image"/>
                <div class="frame-box">
                    <p><span><?php echo $item_name; ?></span></p>
                </div>
<!--                 <img src="<?php echo $item_image; ?>" alt="<?php echo $item_name; ?>" /> -->
                <?php endif; ?>
<!--
                <div class="frame-box">
                    <p><span><?php //echo $item_list[$item_num]['name']; ?></span></p>
                </div>
-->
            </div>
        </div>
    </div>
    <audio id="dram" preload="auto" autoplay>
        <source src="assets/music/tympani-roll1.mp3" type="audio/mp3">
    </audio>
</body>
<script type="text/javascript" src="assets/js/result.js"></script>
</html>
